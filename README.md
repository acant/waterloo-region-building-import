# Waterloo Region Building Import

Scripts, data, and a documentation for the Waterloo Region Building Import into OpenStreetMaps.

The plan for doing this import can be found in the OpenStreetMaps wiki at https://wiki.openstreetmap.org/wiki/Waterloo_Region_Building_Import.

I will use the Waterloo Region boundary from here: https://www.openstreetmap.org/relation/2062151
